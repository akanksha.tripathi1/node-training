"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StartServer = void 0;
const express_1 = __importDefault(require("express"));
const StartServer = () => {
    const app = (0, express_1.default)();
    const { PORT } = process.env;
    app.use((req, res, next) => {
        // console.log(req);
        // console.log({ name: "akanksha", phone: "12344" });
        next();
    });
    app.get("/friends", (req, res, next) => {
        res.send([
            { name: "akanksha", phone: "12344" },
            { name: "niharika", phone: "12344" },
        ]);
    });
    app.listen(PORT, () => {
        console.log(`port started ${PORT}`);
    });
};
exports.StartServer = StartServer;
//preflight check to check whether the browser will response
