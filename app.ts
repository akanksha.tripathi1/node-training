import express, { response } from "express";

export const StartServer = () => {
  const app = express();
  const { PORT } = process.env;

  app.use((req, res, next) => {
    // console.log(req);
    // console.log({ name: "akanksha", phone: "12344" });
    next();
  });

  app.get("/friends", (req, res, next) => {
    res.send([
      { name: "akanksha", phone: "12344" },
      { name: "niharika", phone: "12344" },
    ]);
  });

  app.listen(PORT, () => {
    console.log(`port started ${PORT}`);
  });
};

//preflight check to check whether the browser will response
